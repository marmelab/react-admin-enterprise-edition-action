# React Admin Enterprise Edition action

This CI/CD component sets up your package manager (`npm` or `yarn`) to use the [React Admin Enterprise Edition](https://marmelab.com/ra-enterprise/) private registry, which is required to download the [private modules](https://marmelab.com/ra-enterprise/#private-modules).

It works by creating a `.npmrc` file (or a `.yarnrc.yml` file if you use Yarn v2 or more) and adding the CI authentication token found on the [React Admin Enterprise Edition setup page](https://registry.marmelab.com/setup).

## Inputs

### `RA_EE_CI_TOKEN`

You need to provide your React Admin Enterprise Edition CI authentication token in the `RA_EE_CI_TOKEN` environment variable.

**Notice:** Do not paste it directly in your repository but use a [CI/CD Variable](https://docs.gitlab.com/ee/ci/variables/#define-a-cicd-variable-in-the-ui) instead.

### `stage` *(not mandatory)*

By default this component will execute during the `build` stage. If you want to use it during another stage, you can specify a different `stage` input.

## Example usage

Add this component to an existing `.gitlab-ci.yml` file by using the `include:` keyword.

If your job doesn't include a `before_script` yet:

```yml
include:
- component: gitlab.com/marmelab/react-admin-enterprise-edition-action/component-job@~latest
  inputs:
    RA_EE_CI_TOKEN: $RA_EE_CI_TOKEN

my-job:
  stage: build
  extends: .react-admin-enterprise-edition-action
  script:
    - ...
```

If you already have a `before_script` tag:

```yml
include:
- component: gitlab.com/marmelab/react-admin-enterprise-edition-action/component-job@~latest
  inputs:
    RA_EE_CI_TOKEN: $RA_EE_CI_TOKEN

my-job:
  stage: build
  before_script:
    - !reference [.react-admin-enterprise-edition-action, before_script]
    - ...
  script:
    - ...
```

If you want to specify the `stage` to execute the component:

```yml
include:
- component: gitlab.com/marmelab/react-admin-enterprise-edition-action/component-job@~latest
  inputs:
    RA_EE_CI_TOKEN: $RA_EE_CI_TOKEN
    stage: test

my-job:
  stage: test
  extends: .react-admin-enterprise-edition-action
  script:
    - ...
```

